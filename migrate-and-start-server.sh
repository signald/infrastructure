#!/bin/bash
set -exu

CONFIG_FILE="/etc/signal-server/config.yaml"

# generate config
TEXT_SECURE_SERVER_JAR=/usr/share/TextSecureServer.jar /usr/bin/config-generator "${CONFIG_FILE}"

for db in abusedb accountdb messagedb; do
  echo "Migrating $db"
  java -jar /usr/share/TextSecureServer.jar "$db" migrate "${CONFIG_FILE}"
done
java -jar /usr/share/TextSecureServer.jar server "${CONFIG_FILE}"
