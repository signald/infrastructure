package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func crash(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	db, err := sql.Open("postgres", os.Getenv("DB"))
	crash(err)

	http.HandleFunc("/helper/verification-code", func(w http.ResponseWriter, r *http.Request) {
		account := r.PostFormValue("number")
		var code string
		err := db.QueryRow("SELECT verification_code FROM pending_accounts WHERE number = $1", account).Scan(&code)
		log.Printf("Looking for verification code for %s", account)
		if err != nil {
			panic(err)
		} else {
			fmt.Fprintf(w, "%s\n", code)
		}
	})

	http.HandleFunc("/v2/directory/reconcile", func(w http.ResponseWriter, r *http.Request) {
		_, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		crash(err)
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{\"status\": \"OK\"}\n"))
	})
	log.Fatal(http.ListenAndServe(":8082", nil))
}
