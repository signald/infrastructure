module gitlab.com/signald/test-infrastructure

go 1.14

require (
	github.com/c2h5oh/datasize v0.0.0-20200112174442-28bbd4740fee
	github.com/lib/pq v1.8.0
	gopkg.in/yaml.v2 v2.3.0
)
