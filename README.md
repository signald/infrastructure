# signald test infrastructure

*infrastructure and configuration for signald testing*

## Signal Server
The Signal server is a java app, with dependencies on postgres and redis. `signal-server.Dockerfile` describes
how to build it, and `docker-compose.yaml` shows how to run it. The test server runs nginx in front of the Signal
server, which terminates TLS and proxies back to localhost:8080 (the Signal server)
