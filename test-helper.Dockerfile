FROM golang:latest as golang-build
COPY go.mod go.sum /go/src/gitlab.com/signald/test-infrastructure/
COPY cmd/ /go/src/gitlab.com/signald/test-infrastructure/cmd
WORKDIR /go/src/gitlab.com/signald/test-infrastructure/
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build ./cmd/test-helper

FROM scratch
COPY --from=golang-build /go/src/gitlab.com/signald/test-infrastructure/test-helper /test-helper
ENTRYPOINT ["/test-helper"]
