#!/bin/bash
set -euo pipefail

STORAGE_TIME="${STORAGE_TIME:-604800}"  # Default to 1 week (604800 seconds)

seconds_since() {
    echo $(($(date --utc +%s)-$(date --date="$1" --utc +%s)))
}


prettyexec() {
    echo '\x1b[32;1m$ $@\x1b[0;m'
    $@
}

# Delete .debs that are older than $STORAGE_TIME and in a non-stable branch
for codename in $(mc ls --json -q "$1/dists/" | jq -r .key | grep -v -E "^stable$"); do
    for file in $(mc ls --recursive --json "$1/dists/$codename" | jq -c .); do
        LAST_MODIFIED=$(echo "$file" | jq -r .lastModified)
        FILENAME="$1/dists/$codename$(echo "$file" | jq -r .key)"
        SECONDS_SINCE=$(seconds_since "${LAST_MODIFIED}")
        if [[ "${SECONDS_SINCE}" -gt "${STORAGE_TIME}" ]]; then
            echo "Deleting ${FILENAME}"
            prettyexec mc rm "${FILENAME}"
        fi
    done
done
