FROM debian:latest
ADD apt-signing-key.asc /tmp/apt-signing-key.asc
RUN for p in dpkg-split dpkg-deb tar rm; do ln -s /usr/bin/$p /usr/sbin/$p; done
RUN apt-get update && apt-get install -y \
  gpg \
  dpkg-dev \
  apt-utils \
  wget \
  dh-make \
  debhelper \
  javahelper \
  gradle \
  default-jdk-headless \
  git-buildpackage \
  gradle-debian-helper \
  jq \
  aptly \
  build-essential \
  && rm -rf /var/lib/apt/lists/*

COPY deb-scripts/release-deb.sh /usr/bin/release-deb
COPY deb-scripts/get-component.sh /usr/bin/get-component
COPY deb-scripts/repo-cron.sh /usr/bin/repo-cron

RUN adduser signald
USER signald
RUN gpg --no-default-keyring --keyring trustedkeys.gpg --import /tmp/apt-signing-key.asc
