package main

type WhisperServerConfiguration struct {
	Twilio                 TwilioConfiguration                 `yaml:"twilio"`
	Push                   PushConfiguration                   `yaml:"push"`
	AWSAttachments         S3Configuration                     `yaml:"awsAttachments"`
	GCPAttachments         GCPAttachmentsConfiguration         `yaml:"gcpAttachments"`
	CDN                    S3Configuration                     `yaml:"cdn"`
	Cache                  RedisConfiguration                  `yaml:"cache"`
	Pubsub                 RedisConfiguration                  `yaml:"pubsub"`
	Directory              DirectoryConfiguration              `yaml:"directory"`
	AccountDatabaseCrawler AccountDatabaseCrawlerConfiguration `yaml:"accountDatabaseCrawler"`
	PushScheduler          RedisConfiguration                  `yaml:"pushScheduler"`
	MessageCache           MessageCacheConfiguration           `yaml:"messageCache"`
	MessageStore           DatabaseConfiguration               `yaml:"messageStore"`
	AbuseDatabase          DatabaseConfiguration               `yaml:"abuseDatabase"`
	TestDevices            []TestDeviceConfiguration           `yaml:"testDevices"`
	MaxDevices             []MaxDeviceConfiguration            `yaml:"maxDevices"`
	AccountsDatabase       DatabaseConfiguration               `yaml:"accountsDatabase"`
	// Limits                 RateLimitsConfiguration             `yaml:"limits"`
	// HTTPClient             JerseyClientConfiguration           `yaml:"httpClient"`
	// WebSocket            WebSocketConfiguration            `yaml:"webSocket"`
	Turn                 TurnConfiguration                 `yaml:"turn"`
	GCM                  GCMConfiguration                  `yaml:"gcm"`
	APN                  APNConfiguration                  `yaml:"apn"`
	UnidentifiedDelivery UnidentifiedDeliveryConfiguration `yaml:"unidentifiedDelivery"`
	VoiceVerification    VoiceVerificationConfiguration    `yaml:"voiceVerification"`
	Recaptcha            RecaptchaConfiguration            `yaml:"recaptcha"`
	StorageService       SecureStorageServiceConfiguration `yaml:"storageService"`
	BackupService        SecureBackupServiceConfiguration  `yaml:"backupService"`
	ZKConfig             ZKConfig                          `yaml:"zkConfig"`
	RemoteConfig         RemoteConfigConfiguration         `yaml:"remoteConfig"`
	TransparentDataIndex map[string]string                 `yaml:"transparentDataIndex"`
}

type TwilioConfiguration struct {
	AccountID           string   `yaml:"accountId"`
	AccountToken        string   `yaml:"accountToken"`
	Numbers             []string `yaml:"numbers"`
	LocalDomain         string   `yaml:"localDomain"`
	MessagingServicesID string   `yaml:"messagingServicesId"`
	// CircuitBreaker      CircuitBreakerConfiguration `yaml:"circuitBreaker"`
	// Retry RetryConfiguration `yaml:"retry"`
}

type PushConfiguration struct {
	QueueSize int `yaml:"queueSize"`
}

type S3Configuration struct {
	AccessKey    string `yaml:"accessKey"`
	AccessSecret string `yaml:"accessSecret"`
	Bucket       string `yaml:"bucket"`
	Region       string `yaml:"region"`
}

type GCPAttachmentsConfiguration struct {
	Domain         string `yaml:"domain"`
	Email          string `yaml:"email"`
	MaxSizeInBytes uint64 `yaml:"maxSizeInBytes"`
	PathPrefix     string `yaml:"pathPrefix"`
	RSASigningKey  string `yaml:"rsaSigningKey"`
}

type RedisConfiguration struct {
	URL         string   `yaml:"url"`
	ReplicaURLs []string `yaml:"replicaUrls"`
	// CircuitBreaker CircuitBreakerConfiguration `yaml:"circuitBreaker"`
}

type DirectoryConfiguration struct {
	Redis  RedisConfiguration           `yaml:"redis"`
	SQS    SQSConfiguration             `yaml:"sqs"`
	Client DirectoryClientConfiguration `yaml:"client"`
	Server DirectoryServerConfiguration `yaml:"server"`
}

type AccountDatabaseCrawlerConfiguration struct {
	ChunkSize       uint64 `yaml:"chunkSize"`
	ChunkIntervalMS uint64 `yaml:"chunkIntervalMs"`
}

type MessageCacheConfiguration struct {
	Redis               RedisConfiguration `yaml:"redis"`
	PersistDelayMinutes int                `yaml:"persistDelayMinutes"`
}

type DatabaseConfiguration struct {
	// CircuitBreaker CircuitBreakerConfiguration `yaml:"circuitBreaker"`
	DriverClass string `yaml:"driverClass"`
	Password    string `yaml:"password"`
	URL         string `yaml:"url"`
	User        string `yaml:"user"`
}

type TestDeviceConfiguration struct {
	Number string `yaml:"number"`
	Code   int    `yaml:"code"`
}

type MaxDeviceConfiguration struct {
	Number string `yaml:"number"`
	Count  int    `yaml:"count"`
}

type TurnConfiguration struct {
	Secret string   `yaml:"secret"`
	URIs   []string `yaml:"uris"`
}

type GCMConfiguration struct {
	SenderID int64  `yaml:"senderId"`
	APIKey   string `yaml:"apiKey"`
}

type APNConfiguration struct {
	PushCertificate string `yaml:"pushCertificate"`
	PushKey         string `yaml:"pushKey"`
	BundleID        string `yaml:"bundleId"`
}

type UnidentifiedDeliveryConfiguration struct {
	Certificate string `yaml:"certificate"`
	PrivateKey  string `yaml:"privateKey"`
	ExpiresDays int    `yaml:"expiresDays"`
}

type VoiceVerificationConfiguration struct {
	URL     string   `yaml:"url"`
	Locales []string `yaml:"locales"`
}

type RecaptchaConfiguration struct {
	Secret string `yaml:"secret"`
}

type SecureStorageServiceConfiguration struct {
	UserAuthenticationTokenSharedSecret string `yaml:"userAuthenticationTokenSharedSecret"`
}

type SecureBackupServiceConfiguration struct {
	UserAuthenticationTokenSharedSecret string `yaml:"userAuthenticationTokenSharedSecret"`
}

type ZKConfig struct {
	ServerSecret string `yaml:"serverSecret"`
	ServerPublic string `yaml:"serverPublic"`
	Enabled      bool   `yaml:"enabled"`
}

type RemoteConfigConfiguration struct {
	AuthorizedTokens []string `yaml:"authorizedTokens"`
}

type SQSConfiguration struct {
	AccessKey    string `yaml:"accessKey"`
	AccessSecret string `yaml:"accessSecret"`
	QueueURL     string `yaml:"queueUrl"`
}

// DirectoryClientConfiguration is for interfacing with Contact Discovery Service cluster
type DirectoryClientConfiguration struct {
	// UserAuthenticationTokenSharedSecret is a hex-encoded secret shared with CDS used to generate auth tokens for Signal users
	UserAuthenticationTokenSharedSecret string `yaml:"userAuthenticationTokenSharedSecret"`
	// UserAuthenticationTokenUserIDSecret is a hex-encoded secret shared among Signal-Servers to obscure user phone numbers from CDS
	UserAuthenticationTokenUserIDSecret string `yaml:"userAuthenticationTokenUserIdSecret"`
}

type DirectoryServerConfiguration struct {
	ReplicationURL           string `yaml:"replicationUrl"`
	ReplicationPassword      string `yaml:"replicationPassword"`
	ReplicationCACertificate string `yaml:"replicationCaCertificate"`
}
