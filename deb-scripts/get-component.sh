#!/bin/bash
set -euo pipefail

if [[ "${COMPONENT:-}" == "" ]]; then  # no component specified, we can guess
    if [[ "${CI_BUILD_TAG:-}" == "" ]]; then  # Not building a tag
        if [[ "${CI_COMMIT_REF_SLUG:-}" != "" ]]; then
            COMPONENT="${CI_COMMIT_REF_SLUG}"
        fi
    else
        COMPONENT="stable"
    fi
fi

COMPONENT="${COMPONENT:-experimental}"

echo $COMPONENT
