#!/bin/bash
set -exuo pipefail

## -----------
## Custom repository
## -----------------

## - https://wiki.debian.org/DebianRepository
## - https://debian-handbook.info/browse/stable/sect.setup-apt-package-repository.html
## - https://lists.debian.org/debian-mentors/2006/04/msg00294.html

## -----

COMPONENT=$(get-component)
CODENAME=${CODENAME:-$(lsb_release -sc)}

mkdir -p dists/$CODENAME/$COMPONENT/binary-amd64
# mkdir -p dists/$CODENAME/$COMPONENT/source
find $1 -maxdepth 1 -name "*.deb" -type f -exec cp -a {} dists/$CODENAME/$COMPONENT/binary-amd64 \;
# find $OLDPWD -maxdepth 1 -name "*gradle*xy" -type f -exec cp -a {} dists/$CODENAME/$COMPONENT/source \;

while read; do
    cat > dists/$CODENAME/$COMPONENT/binary-amd64/Release <<-EOF
    Origin: Debian
    Label: Debian
    Suite: $COMPONENT
    Codename: $CODENAME
    Valid-Until: $(date --rfc-2822 --utc --date="${REPO_VALID_UNTIL:-1year}")
    Architectures: amd64
    Components: $COMPONENT
    Description: Debian x.y Unstable - Not Released
EOF
done <<ARCH
binary-amd64
source
ARCH

dpkg-scanpackages -m dists/$CODENAME/$COMPONENT/binary-amd64/ > dists/$CODENAME/$COMPONENT/binary-amd64/Packages
#dpkg-scansources dists/$CODENAME/$COMPONENT/source/ > dists/$CODENAME/$COMPONENT/source/Sources

> dists/$CODENAME/Release apt-ftparchive release dists/$COMPONENT \
    -o APT::FTPArchive::Release::Architectures="amd64 source" \
    -o APT::FTPArchive::Release::Codename="$CODENAME" \
    -o APT::FTPArchive::Release::Components="$COMPONENT" \
    -o APT::FTPArchive::Release::Description="Description: Experimental packages - not released use at your own risk." \
    -o APT::FTPArchive::Release::Label="Debian" \
    -o APT::FTPArchive::Release::Origin="Debian" \
    -o APT::FTPArchive::Release::Suite="$COMPONENT" \


# sign files
gpg --homedir /opt/signing-keys/.gnupg --yes -u $2 --no-tty --passphrase-fd 0 -abs -o dists/$CODENAME/Release.gpg dists/$CODENAME/Release
gpg --homedir /opt/signing-keys/.gnupg --yes -u $2 --no-tty --passphrase-fd 0 --clearsign -o dists/$CODENAME/InRelease dists/$CODENAME/Release


## import signatures on client
# gpg --recv-key --keyserver localhost 0123456789abcdef
# gpg --list-sigs
# gpg --export 0123456789abcdef > /usr/share/keyrings/archive-keyring.gpg
#
# cat >> /etc/apt/sources.list <<apt
# deb [signed-by=/usr/share/keyrings/archive-keyring.gpg] ftp://localhost/debian $(lsb_release -sc) $COMPONENT
# deb-src [signed-by=/usr/share/keyrings/archive-keyring.gpg] ftp://localhost/debian $(lsb_release -sc) $COMPONENT
# apt
## -----
