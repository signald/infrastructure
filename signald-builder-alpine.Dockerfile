FROM alpine:latest

RUN apk upgrade -alU \
    && apk add rust make git cargo \
    && adduser -D signald \
    && rm -rfv /var/cache/apk/*

USER signald
