FROM golang:latest as golang-build
COPY go.mod go.sum /go/src/gitlab.com/signald/test-infrastructure/
COPY cmd/ /go/src/gitlab.com/signald/test-infrastructure/cmd
WORKDIR /go/src/gitlab.com/signald/test-infrastructure/
RUN go mod download
RUN go build ./cmd/config-generator

FROM debian:buster as build
RUN apt-get update && apt-get install -y openjdk-11-jre-headless
RUN apt-get install -y openjdk-11-jdk-headless maven git
RUN git -C /usr/local/src clone https://github.com/signalapp/Signal-Server
WORKDIR /usr/local/src/Signal-Server
COPY signal-server-patches /tmp/signal-server-patches
RUN cat /tmp/signal-server-patches/*.patch | patch -p1
RUN mvn install -DskipTests

FROM debian:buster
RUN apt-get update && apt-get install -y openjdk-11-jre-headless
COPY --from=build /usr/local/src/Signal-Server/service/target/TextSecureServer-*.jar /usr/share/TextSecureServer.jar
COPY --from=golang-build /go/src/gitlab.com/signald/test-infrastructure/config-generator /usr/bin/config-generator
COPY migrate-and-start-server.sh /usr/bin/migrate-and-start-server
RUN mkdir -p /etc/signal-server
RUN useradd signal
RUN chown -R signal /usr/share/TextSecureServer.jar /etc/signal-server
USER signal
CMD ["/usr/bin/migrate-and-start-server"]
