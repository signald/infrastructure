FROM debian:latest
RUN dpkg --add-architecture armhf && dpkg --add-architecture arm64
ADD apt-signing-key.asc /tmp/apt-signing-key.asc
RUN apt-get update && apt-get install -y \
  gpg \
  dpkg-dev \
  apt-utils \
  wget \
  dh-make \
  debhelper \
  javahelper \
  gradle \
  default-jdk-headless \
  git-buildpackage \
  gradle-debian-helper \
  jq \
  aptly \
  build-essential \
  crossbuild-essential-armhf \
  crossbuild-essential-arm64 \
  && rm -rf /var/lib/apt/lists/*

COPY deb-scripts/release-deb.sh /usr/bin/release-deb
COPY deb-scripts/get-component.sh /usr/bin/get-component
COPY deb-scripts/repo-cron.sh /usr/bin/repo-cron

RUN adduser signald
USER signald
RUN gpg --no-default-keyring --keyring trustedkeys.gpg --import /tmp/apt-signing-key.asc
