package main

import (
	"bytes"
	"os"
	"os/exec"
	"strings"
)

func TextSecureServer(command ...string) map[string]string {
	cmd := exec.Command("java", append([]string{"-jar", os.Getenv("TEXT_SECURE_SERVER_JAR")}, command...)...)
	var buf bytes.Buffer
	cmd.Stdout = &buf
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		panic(err)
	}
	out := make(map[string]string)
	for _, line := range strings.Split(buf.String(), "\n") {
		if len(line) == 0 {
			continue
		}
		parts := strings.SplitN(line, ":", 2)
		if len(parts) < 2 {
			continue
		}

		out[strings.TrimSpace(parts[0])] = strings.TrimSpace(parts[1])
	}
	return out
}

func GenerateZKConfig() (z ZKConfig) {
	params := TextSecureServer("zkparams")
	z.ServerPublic = params["Public"]
	z.ServerSecret = params["Private"]
	z.Enabled = false
	return
}

func GenerateUnidentifiedDeliveryConfiguration() (u UnidentifiedDeliveryConfiguration) {
	unidentifiedCA := TextSecureServer("certificate", "--ca")
	unidentifiedKeyPair := TextSecureServer("certificate", "--key", unidentifiedCA["Private key"], "--id", "0")
	u.Certificate = unidentifiedKeyPair["Certificate"]
	u.PrivateKey = unidentifiedKeyPair["Private key"]
	return
}
