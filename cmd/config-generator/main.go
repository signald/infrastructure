package main

import (
	"io"
	"log"
	"os"

	"github.com/c2h5oh/datasize"
	"gopkg.in/yaml.v2"
)

var (
	// configFilename     = "/etc/signal-server/config.yaml"
	caCertificate      string
	defaultRedisConfig = RedisConfiguration{
		URL:         "redis://redis:6379/",
		ReplicaURLs: []string{"redis://redis:6379/"},
	}
	defaultPostgresConfig = DatabaseConfiguration{
		DriverClass: "org.postgresql.Driver",
		Password:    "password",
		URL:         "jdbc:postgresql://db/signal?user=signal&password=password",
		User:        "signal",
	}
)

func main() {
	config := WhisperServerConfiguration{
		Twilio: TwilioConfiguration{
			AccountID:           "a",
			AccountToken:        "a",
			LocalDomain:         "signal-server.invalid",
			MessagingServicesID: "a",
			Numbers:             []string{"+12024561414"},
		},
		Push: PushConfiguration{QueueSize: 100},
		AWSAttachments: S3Configuration{
			AccessKey:    "a",
			AccessSecret: "a",
			Bucket:       "attachments",
			Region:       "us-fake-0",
		},
		GCPAttachments: GCPAttachmentsConfiguration{
			Domain:         "signal-server.invalid",
			Email:          "fake@signal-server.invalid",
			MaxSizeInBytes: 500 * datasize.MB.Bytes(),
			RSASigningKey:  GenerateGCPSigningKey(),
		},
		CDN: S3Configuration{
			AccessKey:    "a",
			AccessSecret: "a",
			Bucket:       "cdn",
			Region:       "us-fake-0",
		},
		Cache:  defaultRedisConfig,
		Pubsub: defaultRedisConfig,
		Directory: DirectoryConfiguration{
			Redis: defaultRedisConfig,
			SQS: SQSConfiguration{
				AccessKey:    "a",
				AccessSecret: "a",
				QueueURL:     "a",
			},
			Client: DirectoryClientConfiguration{
				UserAuthenticationTokenSharedSecret: "00",
				UserAuthenticationTokenUserIDSecret: "00",
			},
			Server: DirectoryServerConfiguration{
				ReplicationURL:           "https://fake.invalid/TODO",
				ReplicationPassword:      "a",
				ReplicationCACertificate: DirectoryReplicationServerCA(),
			},
		},
		AccountDatabaseCrawler: AccountDatabaseCrawlerConfiguration{
			ChunkSize:       datasize.MB.Bytes(),
			ChunkIntervalMS: 1000,
		},
		PushScheduler: defaultRedisConfig,
		MessageCache: MessageCacheConfiguration{
			Redis:               defaultRedisConfig,
			PersistDelayMinutes: 60,
		},
		MessageStore:     defaultPostgresConfig,
		AbuseDatabase:    defaultPostgresConfig,
		TestDevices:      []TestDeviceConfiguration{},
		MaxDevices:       []MaxDeviceConfiguration{},
		AccountsDatabase: defaultPostgresConfig,
		Turn: TurnConfiguration{
			Secret: "a",
			URIs:   []string{"stun:fake.invalid:80"},
		},
		GCM: GCMConfiguration{
			SenderID: 0,
			APIKey:   "fake.invalid",
		},
		APN:                  GenerateAPNConfiguration(),
		UnidentifiedDelivery: GenerateUnidentifiedDeliveryConfiguration(),
		VoiceVerification: VoiceVerificationConfiguration{
			URL:     "https://fake.invalid/voice",
			Locales: []string{"en"},
		},
		Recaptcha:      RecaptchaConfiguration{Secret: "a"},
		StorageService: SecureStorageServiceConfiguration{UserAuthenticationTokenSharedSecret: "00"},
		BackupService:  SecureBackupServiceConfiguration{UserAuthenticationTokenSharedSecret: "00"},
		ZKConfig:       GenerateZKConfig(),
		RemoteConfig: RemoteConfigConfiguration{
			AuthorizedTokens: []string{"a"},
		},
	}

	var writer io.Writer

	writer = os.Stdout

	if len(os.Args) > 1 {
		log.Printf("Writing config to %s", os.Args[1])
		var err error
		f, err := os.Create(os.Args[1])
		if err != nil {
			panic(err)
		}
		defer f.Close()
		writer = f
	}

	err := yaml.NewEncoder(writer).Encode(config)
	if err != nil {
		panic(err)
	}
}
